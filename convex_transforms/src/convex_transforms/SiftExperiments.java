package convex_transforms;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import eu.similarity.msc.core_concepts.CountedMetric;
import eu.similarity.msc.core_concepts.Metric;
import eu.similarity.msc.data.SiftMetricSpace;
import eu.similarity.msc.search.VPTree;

public class SiftExperiments {

	public static void main(String[] args) throws IOException, ClassNotFoundException {

		final SiftMetricSpace sift = new SiftMetricSpace("/Volumes/Data/SIFT_mu/");

		Map<Integer, float[]> siftQueries = sift.getQueries();
		Map<Integer, float[]> siftData = sift.getData();
		List<float[]> rawData = new ArrayList<>();
		rawData.addAll(siftData.values());
		Map<Integer, double[]> thresholds = sift.getThresholds();

		generateCsvFiles(siftQueries, thresholds, rawData, sift.getMetric());
	}

	@SuppressWarnings("boxing")
	private static void generateCsvFiles(Map<Integer, float[]> queries, Map<Integer, double[]> thresholds,
			List<float[]> data, Metric<float[]> metric) throws FileNotFoundException {
		double[] powers = { 1, 1.2, 1.4, 1.6, 1.8, 2, 2.2, 2.4, 2.6, 2.8, 3.0, 3.2, 3.4 };
		int[] results = new int[queries.size()];

		PrintWriter pw = new PrintWriter("/Volumes/Data/nMPTs/siftAll_x.csv");
		pw.println("Pivots,Exp,Recall,Distance Computations");

		for (double power : powers) {
			Metric<float[]> m = getMetric(metric, power);
			CountedMetric<float[]> cm = new CountedMetric<>(m);
			VPTree<float[]> vpt = new VPTree<>(data, cm);
			cm.reset();

			for (int qid : queries.keySet()) {
				double[] queryThresholds = thresholds.get(qid);
				double lastThreshold = Math.pow(queryThresholds[queryThresholds.length - 1], power);
				List<float[]> res = vpt.search(queries.get(qid), lastThreshold);
				if (power == 1) {
					results[qid] = res.size();
				}
				pw.println("," + power + "," + ((float) res.size() / results[qid]) + "," + cm.reset());
				pw.flush();
			}
			pw.println();
		}
		pw.close();
	}

	private static Metric<float[]> getMetric(Metric<float[]> euc, double power) {
		Metric<float[]> m = new Metric<float[]>() {

			@Override
			public double distance(float[] x, float[] y) {
				double d = euc.distance(x, y);
				return Math.pow(d, power);
			}

			@Override
			public String getMetricName() {
				return "euc";
			}
		};
		return m;
	}
}
