package convex_transforms;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import eu.similarity.msc.core_concepts.CountedMetric;
import eu.similarity.msc.data.DataListView;
import eu.similarity.msc.data.DataListView.IdDatumPair;
import eu.similarity.msc.data.MetricSpaceResource;
import eu.similarity.msc.data.SiftMetricSpace;

public class ApplyTransforms {

	public static void main(String[] args) throws ClassNotFoundException, IOException {
		MetricSpaceResource<Integer, float[]> sift = new SiftMetricSpace("/Volumes/Data/SIFT_mu/");
//		MetricSpaceResource<Integer, float[]> mfAlex = new MfAlexMetricSpace("/Volumes/Data/mf_fc6/");
//		MetricSpaceResource<Integer, float[]> euc28 = new EucMetricSpace("/Volumes/Data/euc28/");

		runTransformTest(sift, 50);
	}

	@SuppressWarnings("boxing")
	private static <K, T> void runTransformTest(MetricSpaceResource<K, T> space, int nn)
			throws IOException, ClassNotFoundException {
		final List<IdDatumPair<K, T>> data = DataListView.convert(space.getData());
		final List<IdDatumPair<K, T>> queries = DataListView.convert(space.getQueries());
//		final List<IdDatumPair<K, T>> referencePoints = DataListView.removeRandom(data, 256);
		final Map<K, double[]> thresholds = space.getThresholds();

		final CountedMetric<IdDatumPair<K, T>> cm = DataListView.convert(space.getMetric());
//		LaesaWithTransform<IdDatumPair<K, T>> lae = new LaesaWithTransform<>(data, referencePoints, cm);
		VptWithTransform<IdDatumPair<K, T>> vpt = new VptWithTransform<>(data, cm);
		cm.reset();

		for (IdDatumPair<K, T> query : queries) {
			final double[] ts = thresholds.get(query.id);
			final double threshold = ts[nn - 1];

			System.out.print(query.id + "\t" + threshold);
			int trueResults = 0;
			for (float f = 2f; f < 3.5; f += 0.2) {
				final double pow = f;
				vpt.setTransform((x) -> Math.pow(x, pow));
				final List<IdDatumPair<K, T>> res = vpt.search(query, threshold);
				if (f == 1) {
					trueResults = res.size();
					System.out.print("\t" + trueResults);
				}
				System.out.print("\t" + (float) res.size() / trueResults + "\t" + cm.reset());
			}
			System.out.println();
		}
	}
}
